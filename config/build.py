# -*- coding: utf-8 -*-
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# %                 Build script for Displam                     %
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
"""
Build script to create a platfrom and Python dependent wheel.
 
@note: Displam build script                 
Created on 29.11.2023    

@version:  1.0    
----------------------------------------------------------------------------------------------
@requires:
       - 

@change: 
       -    
   

@author: garb_ma                                                     [DLR-FA,STM Braunschweig]
----------------------------------------------------------------------------------------------
"""

import os, sys
import site
import subprocess
import tempfile
import shutil

def build(*args, **kwargs):
    """
    This is the main entry point.
    """
    # Add internal build dependency resolver to current path
    site.addsitedir(site.getusersitepackages())
    # Import build tools
    try: from PyXMake import Command
    except: raise ImportError("Cannot import PyXMake. Skipping compilation")
    # Define source and output directory
    source = os.path.join(os.getcwd(),"src","displam_Quellcode")
    config = os.path.join(os.getcwd(),"config")
    output = os.path.join(os.getcwd(),"bin","displam")
    # Always create a fresh install
    if os.path.exists(output): shutil.rmtree(output)
    # Define scratch folder
    scratch = tempfile.mkdtemp()
    # Create ouput directory 
    os.makedirs(os.path.join(output,"bin"))
    # This are all mandatory files for compilation
    files = os.listdir(source)
    # Copy all required files to the output directory
    shutil.copy(os.path.join(config,"__init__.py"),os.path.join(output,"cli.py"))
    # Create a proper namespace for displam 
    open(os.path.join(output,"__init__.py"),'a+').close()
    # Execute build command
    Command.cxx("displam", files=files, source=source, include=[], output=os.path.join(output,"bin"), scratch=scratch, verbosity=2)
    # Delete scrath folder
    shutil.rmtree(scratch)
    pass
    
def main(): 
    """
    This is the main entry point.
    """
    return build()

if __name__ == "__main__":
    main(); sys.exit()
