# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [ [1.5.1](https://gitlab.com/DLR-SY/displam/-/tags/v1.5.1) ] - 2024-08-24
### Changed
- Updated repository and maintainer info

## [ [1.5.0](https://gitlab.com/DLR-SY/displam/-/tags/v1.5.0) ] - 2024-07-02
### Changed
- Public release
- Fixing wheels for musllinux builds

## [ [1.4.2](https://gitlab.com/DLR-SY/displam/-/tags/v1.4.2) ] - 2024-07-01
### Changed
- Fixing issues in internal upstream CI/CD pipeline configuration files
- Updating authorship
- Fixing zenodo export 

## [ [1.4.1](https://gitlab.com/DLR-SY/displam/-/tags/v1.4.1) ] - 2024-05-16
### Changed
- Fixing issues in public CI/CD pipeline

## [ [1.4.0](https://gitlab.com/DLR-SY/displam/-/tags/v1.4.0) ] - 2024-05-15
### Changed
- Public release
- Updating changelog

## [ [1.3.0](https://gitlab.dlr.de/groups/fa_sw/-/packages/9185) ] - 2023-03-14
### Changed
- Fixing internal and external CI/CD pipelines

## [ [1.2.1](https://gitlab.dlr.de/groups/fa_sw/-/packages/8838) ] - 2024-02-24
### Changed
- Minor bug fixes

## [ [1.2.0](https://gitlab.dlr.de/groups/fa_sw/-/packages/8748) ] - 2024-02-19
### Changed
- Updating 3rd party dependencies
- Fixing encoding issues on windows

## [ [1.1.0](https://gitlab.dlr.de/groups/fa_sw/-/packages/8013) ] - 2023-12-06
### Changed
- Added poetry build support

## [ [1.0.1](https://gitlab.dlr.de/groups/fa_sw/-/packages/7961) ] - 2023-12-06
### Changed
- Added support for legacy python interpreters on Windows.
- Fixed CLI script

## [ [1.0.0](https://gitlab.dlr.de/groups/fa_sw/-/packages/7945) ] - 2023-12-05
### Changed
- Initial wheel support for all platforms
