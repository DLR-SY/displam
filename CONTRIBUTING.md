## Contributing
> If it is the first time that you contribute, please add yourself to the list
> of contributors below.

Contributions are always welcomed!

If you want to provide a code change, please:

* Create a clone or fork of the GitLab project in question.
* Develop the feature/patch
* Provide a merge request.

## Authors
* Jens Baaran
* Marc Garbade
* Florian Raddatz
* Daniel Schmidt
* Maria Moix-Monet
